#!/usr/bin/env bash

# *******************************************************************************
# BEING CONFIGURATION
# Please consult your S3 provider for how to get the S3 items below
# *******************************************************************************

# our AWS Key 
S3_KEY="";

# our AWS Secret 
S3_SECRET="";

# our S3 Bucket 
S3_BUCKET="";

# our S3 Endpoint, JUST THE FQDN, for example AWS is: s3.amazonaws.com 
S3_ENDPOINT="";

# our S3 Region 
S3_REGION="";

# our backup repos, and their associated hashes, if you have more for this
# add a comma after the last line, and add new nodes as needed
BACKUP_REPOS='''
[
    {"name":"THE BACKUPS NAME", "hash":"THE BACKUPS HASH"}
]
'''

# *******************************************************************************
# END CONFIGURATION
# NO NEED TO TOUCH ANYTHING ELSE BELOW THIS LINE !
# *******************************************************************************

# our backup types
BACKUP_TYPES=( "Application" "Database" "Other" )

# backup type menu
backup_type_menu( ) {

    # loop the backup types
    select choice in "${BACKUP_TYPES[@]}"; do
        
        # the selectable menu item
        [[ -n $choice ]] || { echo "Invalid choice. Please try again." >&2; continue; }
        break; # valid choice was made; exit prompt.
    done;

    # read the selected input
    read -r _type <<<"$choice";

    # return the type selecetd
    BACKUP_TYPE=$_type;

}

# generate the server menu
server_menu( ) {

    # hold some arrays
    declare -A _name _hash;

    while read -r _server _pw; do

        # hold the selected item
        _name[$_server]=$_server;
        _hash[$_server]=$_pw;

    # end the loop generation
    done < <( jq -rc '.[] | "\(.name) \(.hash)"' <<< "$BACKUP_REPOS" )

    # now loop the resultset
    select _server in "${!_name[@]}"; do 
        [ -n "$_server" ] && break;
    done;

    # return the server and hash
    SELECTED_SERVER=${_name[$_server]};
    SELECTED_HASH=${_hash[$_server]};

}

repo_menu( ) {

    # set the end path based on the selected backup type
    if [[ $BACKUP_TYPE == "Application" ]]; then
        THE_PATH="apps";
    elif [[ $BACKUP_TYPE == "Database" ]]; then
        THE_PATH="database";
    elif [[ $BACKUP_TYPE == "Other" ]]; then
        THE_PATH="other";
    fi;

    # get the list of the backups
    REPO_LIST=`aws s3 ls s3://$S3_BUCKET/$SELECTED_SERVER/$THE_PATH/`

    # declare a string array
    RET_ARR=( )

    # loop over this list and grab only the names
    for ITEM in $REPO_LIST; do

        # make sure this isn't a PRE
        if [[ $ITEM != "PRE" ]]; then

            # append to the return array removing the last /
            RET_ARR+=( "${ITEM::len-1}" );

        fi;
    done;

    # loop the return array
    select choice in "${RET_ARR[@]}"; do
        
        # the selectable menu item
        [[ -n $choice ]] || { echo "Invalid choice. Please try again." >&2; continue; }
        break; # valid choice was made; exit prompt.
    done;

    # read the selected input
    read -r _type <<<"$choice";

    # if it's an application, we need to go one step further... we need to browse the accounts for selecting
    # so perform another s3 ls lookup inside the selected account
    if [[ $BACKUP_TYPE == "Application" ]]; then

        echo
        echo "*************************************************************************";
        echo "Select the application to restore from:";
        echo "*************************************************************************";

        # get the list of the apps
        APP_LIST=`aws s3 ls s3://$S3_BUCKET/$SELECTED_SERVER/$THE_PATH/$_type`;
        
        # declare a string array
        APP_ARR=( )

        # loop over this list and grab only the names
        for ITEM in $APP_LIST; do

            # make sure this isn't a PRE
            if [[ $ITEM != "PRE" ]]; then

                # append to the return array removing the last /
                APP_ARR+=( "${ITEM::len-1}" );

            fi;
        done;

        # loop the return array
        select choice in "${APP_ARR[@]}"; do
            
            # the selectable menu item
            [[ -n $choice ]] || { echo "Invalid choice. Please try again." >&2; continue; }
            break; # valid choice was made; exit prompt.
        done;

        # read the selected input
        read -r _app <<<"$choice";

        # return the app selecetd
        BACKUP_APP=$_app;

    fi;

    # return the type selecetd
    BACKUP_NAME=$_type;

}
