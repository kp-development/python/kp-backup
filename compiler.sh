#!/usr/bin/env bash

# get the version number
TMPVER=`cat /etc/os-release | grep VERSION_ID`
VER=$(echo "$TMPVER" | sed "s/VERSION_ID=//")
VER=${VER:1:-1}

# make sure pip is indeed installed
apt-get install -y python3-pip

# make sure zip is installed
apt-get install zip

# let's make sure pip is up to date
python3 -m pip install --upgrade pip

# now we need to make sure that python3-dev
apt-get install -y python3-dev

# let's install pyinstaller
pip install --upgrade pyinstaller

# get the path to this script
CODEPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )";

# make a directory for the releases
mkdir -p $CODEPATH/release;

# try to compile
pyinstaller \
    --distpath $CODEPATH/release/ \
    --clean \
    -F \
    -n kpbr-$VER \
    -p $CODEPATH/source/ \
$CODEPATH/source/kpbr.py

# find and remove the PYC files
find . -type f -name "*.pyc" -exec rm -f {} \;
find . -type d -name "__pycache*" -exec rm -rf {} \;

# remove the build directory
rm -rf $CODEPATH/build

# set the executable bit
chmod +x $CODEPATH/release/kpbr-$VER

# now zip up the mount scripts
zip mount-scripts.zip mount-backup.sh .mb-source.sh 1> /dev/null

# move the zip to the release folder
mv mount-scripts.zip $CODEPATH/release/
