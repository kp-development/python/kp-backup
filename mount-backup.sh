#!/usr/bin/env bash

# *******************************************************************************
# Setup everything we need to run this
# *******************************************************************************

# we must be the root user, we need to be able to create the mount directory, as well as be able to mount
if [[ $USER != "root" ]]; then 
    echo "This script must be run as root"; 
    exit 1;
fi;

# requirements met
REQ_MET=true;

# make sure restic is actually installed
if ! command -v restic &> /dev/null; then
    echo "*************************************************************************";
    echo "'restic' is not installed on this system";
    echo "Please see here for instructions on how to install it for your system."
    echo "https://restic.readthedocs.io/en/latest/020_installation.html"
    echo "*************************************************************************";
    REQ_MET=false;
fi;

# make sure aws-cli is installed
if ! command -v aws &> /dev/null; then
    echo "*************************************************************************";
    echo "'aws cli' is not installed on this system";
    echo "Please see here for instructions on how to install it for your system."
    echo "https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html"
    echo "Please note, you should either move the binary or create a symlink to it"
    echo "in /usr/bin/"
    echo "*************************************************************************";
    REQ_MET=false;
fi;

# make sure jq is installed
if ! command -v jq &> /dev/null; then
    echo "*************************************************************************";
    echo "'jq' is not installed on this system";
    echo "Please see here for instructions on how to install it for your system."
    echo "https://stedolan.github.io/jq/"
    echo "*************************************************************************";
    REQ_MET=false;
fi;

# if the requirements are NOT met, exit
if ! $REQ_MET; then

    # exit
    exit 1;
fi;

# include our source file.  file contains some config items and functions for this main file
. $(dirname "$0")/.mb-source.sh

# *******************************************************************************
# End the setup
# *******************************************************************************

# set the neccessary environment variables
export AWS_ACCESS_KEY_ID=$S3_KEY;
export AWS_SECRET_ACCESS_KEY=$S3_SECRET;
export AWS_DEFAULT_REGION=$S3_REGION;

# show a message stating we're going to prompt some questions
echo
echo "*************************************************************************";
echo "You will now be asked for the backups information."
echo "Please pay attention and select the backup you want to browse."
echo "*************************************************************************";
sleep 3

# *******************************************************************************
# Here we are going to be prompting for the backup name/server
# Once we have that, we'll prompt for the type of backup
# Once we have that we'll prompt for either the app name, database name, or 
# have the enduser type in the orginal path (for OTHER)
# *******************************************************************************

# show the server selection menu
echo
echo "*************************************************************************";
echo "Select the server to restore from:";
echo "*************************************************************************";
server_menu;

echo
echo "*************************************************************************";
echo "Select the type of backup to restore from:";
echo "*************************************************************************";
backup_type_menu;

# now that we have these we can query s3cmd to list all backup repos in the bucket
echo
echo "*************************************************************************";
echo "Select the backup to restore from:";
echo "*************************************************************************";
repo_menu;

# *******************************************************************************
# This is where all the funky stuff actually happens to mount the backup
# *******************************************************************************

# setup the repo prefix
REPO_PREFIX="s3://$S3_ENDPOINT/$S3_BUCKET"

# export the restic backup hash
export RESTIC_PASSWORD=$SELECTED_HASH;

# build the full repo URL based on the backup type
if [ $BACKUP_TYPE == "Database" ]; then

    # database backup repo
    BU_REPO="$SELECTED_SERVER/database/$BACKUP_NAME";

elif [ $BACKUP_TYPE == "Application" ]; then

    # application backup repo
    BU_REPO="$SELECTED_SERVER/apps/$BACKUP_NAME/$BACKUP_APP";

elif [ $BACKUP_TYPE == "Other" ]; then

    # other path backup repo
    BU_REPO="$SELECTED_SERVER/other/$BACKUP_NAME"

fi;

# set a temporary mount location for this
TMP_MOUNT="/tmp/backup-mount";

# let's try to just browse the entire backupset?
REPO="$REPO_PREFIX/$BU_REPO/";

# create a temporary mount for this
mkdir -p $TMP_MOUNT;

# show a message for the exact location of the snapshots
echo ;
echo "*************************************************************************";
echo "Please leave this open while you view the files in the backup snapshot.";
echo "Browse to: $TMP_MOUNT/snapshots/ to view";
echo "Press CTRL-C to unmount and quit.";
echo "*************************************************************************";

# mount the backup, but don't show the standard output
restic -r $REPO mount $TMP_MOUNT 1> /dev/null;

# unset the environment variables
unset AWS_DEFAULT_REGION;
unset AWS_ACCESS_KEY_ID;
unset AWS_SECRET_ACCESS_KEY;
unset RESTIC_PASSWORD;
