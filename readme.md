# KP Backup and Restore

## REQUIREMENTS

- **Operating System**
    - Ubuntu 18.04+
    - Mac OSX `UNTESTED`
    - Windows 10
        - Works and tested in WSL Ubuntu 18.04 & Ubuntu 20.04
            - Minor visual glitch with the select menus that go away if yuo go up and down once
        - `UNTESTED in CYGWIN`

- **Server Software & Binaries**
    - **NOTE:** Do not run `kpbr -a setup` on Mac or Windows, instead please look up the proper documentation to install the proceeding packages for your system.
    - Python3.6+
    - Python dateutil module
        - for backing up & restore
    - restic
        - for backing up & restore
        - for restore/mount script
    - boto3
        - for backing up & restore
    - aws-cli
        - for restore selection
        - for restore/mount script
    - jq
        - for restore/mount script
    - mysql or mariadb
        - for backing up

## INSTALL / UPDATE

For ubuntu, download the release binary associated to your flavor from here: 
- https://kevinpirnie.com/kp/kpbr-18.04 <- Ubuntu 18.04
- https://kevinpirnie.com/kp/kpbr-20.04 <- Ubuntu 20.04

rename it to `kpbr`, make it executable, and move it to `/usr/bin/`

Once there, run `sudo kpbr -a setup`, it will install or update everything your server will need to run.

- All in one command: `sudo wget https://kevinpirnie.com/kp/kpbr-18.04 -O kpbr && sudo chmod +x kpbr && sudo mv kpbr /usr/bin/ && sudo kpbr -a setup` <- for Ubuntu 18.04
- All in one command: `sudo wget https://kevinpirnie.com/kp/kpbr-20.04 -O kpbr && sudo chmod +x kpbr && sudo mv kpbr /usr/bin/ && sudo kpbr -a setup` <- for Ubuntu 20.04

### BACKUP MOUNT

Download and unzip the `mount-scripts.zip` from here: `https://kevinpirnie.com/kp/mount-scripts.zip`.  You may need to make the 2 files executable.   

Once expanded open `.mb-source.sh` and configure it.  For the BACKUP_REPOS, it is a JSON string array.  Follow the example to create more nodes.

## USAGE - `kpbr`

    kpbr -a [setup|config|backup] [ARGS]

    Required Arguments Per Action:

        setup:
            NONE
                Will prompt you to configure or re-configure once the setup has been completed.

        config:
            NONE
                Will ask you questions to configure the app.  Please pay attention carefully.

        backup:
            --what [all|db|database|acct|account|app|other]
                all: 
                    NONE - also performs a full database backup
                db/database:
                    --db or --database THE DATABASE NAME | ALL - REQUIRED 
                acct/account:
                    --acct or --account THE ACCOUNT NAME - REQUIRED 
                app:
                    --acct or --account THE ACCOUNT NAME - REQUIRED 
                    --app THE APP NAME - REQUIRED 
                other:
                    --paths COMMA-DELIMITED STRING OF FULL PATHS - REQUIRED 
                NOTE:
                    --paths can also be passed for all|db/database|acct/account|app as well to add additional paths to backup

        restore:
            NONE: Everything will be prompted for you.
            PROMPTS:
                Backup Location:
                    Local: this is the local server's backup.  It will pull everything needed from the configured config file
                    Remote: this is a remote servers backup.  You will have to enter the S3 API Key, S3 API Secret, S3 Endpoint, S3 Bucket, S3 Region, backup name/server, and the backup hash.   Make sure you have all this information handy, it is all required.
                Type of Backup:
                    Application: Otherwise known as the web application, set of files to be restored.
                        Addition Prompts:
                            Account: The account to be restored
                            Application: The application in the chosen account to be restored
                            Snapshot: The specific backup point you would like to restore.
                    Database: The database to be restored
                        Addition Prompts:
                            Database: The account to be restored
                            Snapshot: The specific backup point you would like to restore.
                Restore Method:
                    Automatic: This will overwrite the originals, so make sure you really want to do this.
                    Manual: This will restore the files to the specified path.  This includes the mysqldump if Database has been chosen as the type.
                        Addition Prompts:
                            Restore Path: The files will be restored to this path and be left alone for you to do with them as you see fit. The default is `/tmp/restore`
                    Browse: This will mount the backup as a network drive, allowing you to browse the files as you see fit.  The default mount point is `/tmp/backup-mount` however you can specify whatever path you see fit.
                        Addition Prompts:
                            Mount Point: The mount point that the backup will be mounted to, allowing you to browse the files as you see fit.  The default mount point is `/tmp/backup-mount`

## USAGE - MOUNT SCRIPTS

You can mount a backup as a networked drive.  

Configure by openning `.mb-source.sh` and entering your AWS Key and Secret, the S3 Bucket, S3 Region, and S3 Endpoint (for aws this is s3.amazonaws.com).

Then add your server and server hash to the json nodes.  `name` = the server name, `hash` = the server hash

Once configured run the script `mount-backup.sh` then browse to `/tmp/backup-mount/` on your machine.  Inside it will contain the backups you selected.  `snapshots` is the timestamped snaphots of the specified backup, with a symlink to the `latest`
