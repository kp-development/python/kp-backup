#!/usr/bin/python3

# some necessary imports
import os, argparse, sys, time
from argparse import RawTextHelpFormatter

# import the common class
from work.common import KP_Common
common = KP_Common( )

# get the python version.    This will only work with v3.6+
_py_ver = sys.version_info
if _py_ver[0] < 3 and _py_ver[1] < 6:
    print( "*" * 52 )
    print ( "You must be running at least Python 3.6" )
    print( "You are currently running version: {}.{}.{}\n".format( _py_ver[0], _py_ver[1], _py_ver[2] ) )
    print( "*" * 52 )
    sys.exit( )

# make sure we are a sudo user before we go any further
is_su = os.getuid( )
if is_su != 0:
    print( "*" * 52 )
    print ( "You must run this as root, or at least a sudo user.\nPlease login as the root/sudo account and try again.\n" )
    print( "*" * 52 )
    sys.exit( )

# fire up the argument parser
_args = argparse.ArgumentParser( formatter_class=RawTextHelpFormatter, usage='''
\033[94mkpbr -a [setup|config|backup|restore] [ARGS]\033[37m

Required Arguments Per Action:

    \033[92msetup\033[37m:
        \033[94mNONE\033[37m
            Will prompt you to configure or re-configure once the setup has been completed.

    \033[92mconfig\033[37m:
        \033[94mNONE\033[37m
            Will ask you questions to configure the app.  Please pay attention carefully.

    \033[92mbackup\033[37m:
        \033[94m--what [all|db|database|acct|account|app|other]\033[37m
            \033[94mall\033[37m: 
                NONE - also performs a full database backup
            \033[94mdb/database\033[37m:
                \033[94m--db\033[37m or \033[94m--database\033[37m THE DATABASE NAME | ALL - \033[92mREQUIRED\033[37m 
            \033[94macct/account\033[37m:
                \033[94m--acct\033[37m or \033[94m--account\033[37m THE ACCOUNT NAME - \033[92mREQUIRED\033[37m 
            \033[94mapp\033[37m:
                \033[94m--acct\033[37m or \033[94m--account\033[37m THE ACCOUNT NAME - \033[92mREQUIRED\033[37m 
                \033[94m--app\033[37m THE APP NAME - \033[92mREQUIRED\033[37m 
            \033[94mother\033[37m:
                \033[94m--paths\033[37m COMMA-DELIMITED STRING OF FULL PATHS - \033[92mREQUIRED\033[37m 
            \033[94mNOTE\033[37m:
                \033[94m--paths\033[37m can also be passed for all|db/database|acct/account|app as well to add additional paths to backup

    \033[92mrestore:\033[37m
        \033[94mNONE\033[37m
            Fully interactive restore.  Please pay attention

''', allow_abbrev=False, add_help=False )

# we need an action argument at the very least
_args.add_argument( "-a", dest='action', choices=[ "setup", "config", "backup", "restore" ], help=argparse.SUPPRESS )

# backup arguments: --what {all,db,account,app,other}; db/database; acct/account; app & acct/account; path
_args.add_argument( "--what", choices=[ "all", "db", "database", "acct", "account", "app", "other" ], help=argparse.SUPPRESS )
_args.add_argument( "--paths", help=argparse.SUPPRESS )
_args.add_argument( "--acct", "--account", dest="account", help=argparse.SUPPRESS )
_args.add_argument( "--app", help=argparse.SUPPRESS )
_args.add_argument( "--db", "--database", dest="database", help=argparse.SUPPRESS )
_args.add_argument( "--hash", help=argparse.SUPPRESS )
_args.add_argument( "--name", help=argparse.SUPPRESS )

# if no arguments have been passed, quit, and show the help
if not len( sys.argv ) > 1:

    print( "*" * 52 )
    common.kp_print( "error", "You must pass at least 1 argument." )
    print( "*" * 52 )
    _args.print_help( )
    print( "*" * 52 )
    sys.exit( )

# now that we know we have an action, parse
_the_args = _args.parse_args( )

# wrap all the actions in a try block
try:

    # if we are performing the setup
    if _the_args.action.lower( ) == "setup":

        # we don't need any option arguments for this, so just fire up the config class and do it!
        from work.setup import KP_Setup
        _setup = KP_Setup( )

        # run the configurator
        _setup.run( )

        # clean up
        del _setup

    # if we are performing a config
    if _the_args.action.lower( ) == "config":

        # we don't need any option arguments for this, so just fire up the config class and do it!
        from work.config import KP_Config
        _config = KP_Config( )

        # run the configurator
        _config.run( )

        # clean up
        del _config

    # backup
    if _the_args.action.lower( ) == "backup":
        
        # fire up the backup class
        from work.backup import KP_Backup
        _bu = KP_Backup( _args )

        # run it
        _bu.run( )

        # clean up
        del _bu
        
    # restore
    if _the_args.action.lower( ) == "restore":
        
        # fire up the restore class
        from work.restore import KP_Restore
        _r = KP_Restore( )

        # run it
        _r.run( )

        # clean up
        del _r

# catch the keyboard interrupt
except KeyboardInterrupt:

    # show a message then exit
    print
    print( "*" * 52 )
    common.kp_print( "info", "Exitting the app, please hold." )
    print( "*" * 52 )
    time.sleep( 5 )
    sys.exit( )

