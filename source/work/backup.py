#!/usr/bin/python3

# some necessary imports
import sys, os, glob

class KP_Backup:

    # initialize
    def __init__( self, _args = None ):

        # let's import our common class
        from work.common import KP_Common

        # now throw it into a class wide variable for use
        self.common = KP_Common( )

        # global class arguments as passed from the main app
        self.args = _args

        # global class parsed arguments
        if _args:
            the_args = _args.parse_args( )

            # setup the arguments for local class use
            
            # what
            self.what = self.common.arg_to_lower( the_args.what )
            
            # database
            self.database = self.common.arg_to_lower( the_args.database )

            # account
            self.account = self.common.arg_to_lower( the_args.account )

            # app
            self.app = self.common.arg_to_lower( the_args.app )

            # paths
            self.paths = the_args.paths

    # clean up
    def __del__( self ):

        # clean up the common class
        del self.common, self.args

    def run( self ):

        # make sure we have a backup argument
        if self.what is None:

            # show an error message then exit
            print( "*" * 52 )
            self.common.kp_print( "error", "You must pass at least 1 backup argument." )
            print( "*" * 52 )
            self.args.print_help( )
            print( "*" * 52 )
            sys.exit( )

        # show a starting message
        print( "*" * 52 )
        self.common.kp_print( "info", "Please hold while we run your backup." )
        print( "*" * 52 )

        # set some environment variables
        os.environ["AWS_ACCESS_KEY_ID"] = self.common.key
        os.environ["AWS_SECRET_ACCESS_KEY"] = self.common.secret
        os.environ["AWS_DEFAULT_REGION"] = self.common.region
        os.environ["RESTIC_PASSWORD"] = self.common.hash

        # we need to make sure we have the necessary backup actions
        if ( self.what == "db" ) or ( self.what == "database" ):

            if not self.database:

                # throw an error message then exit
                print( "*" * 52 )
                self.common.kp_print( "error", "Please pass the database name or ALL to the --db or --database argument." )
                print( "*" * 52 )
                sys.exit( )
            
            # run the database backup
            self.__run_database_backup( self.database.lower( ) )

            # run the other backup
            self.__run_other_backup( self.paths )

        elif self.what == "all":

            # run the database backup for all databases
            self.__run_database_backup( "all" )

            # setup the path we're backing up here
            _path = "{}*/{}/*".format( self.common.path_start, self.common.path_for_apps )

            # get the true path we need to loop
            _true_path = glob.glob( _path )

            # do this concurrently
            self.common.concurrent_process( self.__run_all_file_backup, _true_path )

            # process the "extra" path(s) backups
            self.__run_other_backup( self.paths )

        elif ( self.what == "acct" ) or ( self.what == "account" ):

            # if the account flag does not exist
            if not self.account:

                # show an error message then exit
                print( "*" * 52 )
                self.common.kp_print( "error", "You must pass the account argument." )
                print( "*" * 52 )
                self.args.print_help( )
                print( "*" * 52 )
                sys.exit( )

            # setup the backup path
            _path = "{}{}/{}/*".format( self.common.path_start, self.account, self.common.path_for_apps )

            # the true path
            _true_path = glob.glob( _path )

            # concurrent backup
            self.common.concurrent_process( self.__run_acct_backup, _true_path, account=self.account )

            # process the "extra" path(s) backups
            self.__run_other_backup( self.paths )

        elif self.what == "app":

            # if the account or app flags do not exist
            if ( not self.account ) or ( not self.app ):

                # show an error message then exit
                print( "*" * 52 )
                self.common.kp_print( "error", "You must pass both the account and app arguments." )
                print( "*" * 52 )
                self.args.print_help( )
                print( "*" * 52 )
                sys.exit( )

            # setup the backup path
            _path = "{}{}/{}/{}".format( self.common.path_start, self.account, self.common.path_for_apps, self.app )

            # run the backup
            self.__run_app_backup( _path, self.account, self.app )

            # process the "extra" path(s) backups
            self.__run_other_backup( self.paths )
            
        elif self.what == "other":

            # make sure the paths argument is set
            if not self.paths:

                # show an error message then exit
                print( "*" * 52 )
                self.common.kp_print( "error", "You must pass the paths argument." )
                print( "*" * 52 )
                self.args.print_help( )
                print( "*" * 52 )
                sys.exit( )

            # backup the paths
            self.__run_other_backup( self.paths )

        # remove the environment variables
        del os.environ['AWS_ACCESS_KEY_ID']
        del os.environ['AWS_SECRET_ACCESS_KEY']
        del os.environ['AWS_DEFAULT_REGION']
        del os.environ['RESTIC_PASSWORD']

        print( "*" * 52 )
        self.common.kp_print( "success", "The backup has been completed." )
        print( "*" * 52 )
        sys.exit( )

    # do the work to backup the database(s)
    def __run_database_backup( self, _which ):

        # open the connection to our configured database server
        _db = self.__db_server_connection( )

        # setup the cursor
        _cur = _db.cursor( )

        # the main mysqldump command
        _the_command = self.__mysqldump_command( ) + " {}"

        # check if we're backing up all databases
        if _which == "all":

            # execute
            _cur.execute( "SHOW DATABASES;" )

            # get the resultset
            _rs = _cur.fetchall( )

            # make sure we have a resultset
            if _rs:

                # concurrently backup the databases
                self.common.concurrent_process( self.__cc_db_backup, _rs )

        # otherwise it's a single database
        else:

            # setup the destination
            _dest = "{}{}/".format( self.common.backup_path_db, _which )

            # the backup command needs to be a bit different than the norm
            _bu_cmd = "{} | {} backup --stdin --stdin-filename {}.sql".format( 
                _the_command.format( _which ),
                self.common.primary_command.format( _dest ),
                _which
            )

            # initialize if we need to
            self.common.backup_init( _dest )

            # run the backup command
            self.common.execute( _bu_cmd )

            # clean up the backup
            self.common.backup_cleanup( _dest )

        # close the cursor
        _cur.close( )

        # close the database server connection
        _db.close( )

    # the method run to concurrrently backup all databases
    def __cc_db_backup( self, _row ):

        # the main mysqldump command
        _the_command = self.__mysqldump_command( ) + " {}"

        # not the system databases
        if not ( _row[0] == "sys" or _row[0] =="mysql" or _row[0] == "information_schema" or _row[0] == "performance_schema" ):
            
            # setup the destinatio
            _dest = "{}{}/".format( self.common.backup_path_db, _row[0] )

            # the backup command needs to be a bit different than the norm
            _bu_cmd = "{} | {} backup --stdin --stdin-filename {}.sql".format( 
                _the_command.format( _row[0] ),
                self.common.primary_command.format( _dest ),
                _row[0]
            )

            # initialize if we need to
            self.common.backup_init( _dest )

            # run the backup command
            self.common.execute( _bu_cmd )

            # clean up the backup
            self.common.backup_cleanup( _dest )

    # setup the mysqldump command
    def __mysqldump_command( self ):

        # hold our command
        _cmd = "mysqldump {} --single-transaction --routines --no-create-db "

        # let's see if we have a defaults file
        if self.common.mysql_defaults:

            # we do, return the string with the defaults file appended
            return _cmd.format( "--defaults-file={}".format( self.common.mysql_defaults ) )
        
        # we don't, so test for a username and password
        elif ( self.common.mysql_user is not None ) and ( self.common.mysql_password is not None ):

            # we do, return the string with the user and password appended
            return _cmd.format( "-u {} -p{}".format( self.common.mysql_user, self.common.mysql_password ) )

        # we have nothing, so give it a shot.
        else:

            # return the string with no formatting
            return _cmd

    # setup the database server connection
    def __db_server_connection( self ):

        # import the mysql module
        import MySQLdb

        # hold our db connection handle
        _dbch = None

        # let's see if we have a defaults file
        if self.common.mysql_defaults:

            # we do, use it to connect
            _dbch = MySQLdb.connect( self.common.mysql_host, read_default_file=self.common.mysql_defaults )
        
        # we don't, so test for a username and password
        elif ( self.common.mysql_user is not None ) and ( self.common.mysql_password is not None ):

            # we do, use them to connect
            _dbch = MySQLdb.connect( self.common.mysql_host, self.common.mysql_user, self.common.mysql_password )

        # we have nothing, so give it a shot.
        else:

            # try to connect without
            _dbch = MySQLdb.connect( self.common.mysql_host )

        # return the connection handle
        return _dbch

    # do the work to run the app backup
    def __run_app_backup( self, _path, _acct, _app ):

        # setup the destination
        _dest = "{}{}/{}".format( self.common.backup_path_app, _acct, _app )

        # initialize if we need to
        self.common.backup_init( _dest )

        # run the backup
        self.common.backup_run( _dest, _path )

        # clean up the backup
        self.common.backup_cleanup( _dest )

    # do the work to run the account backup
    def __run_acct_backup( self, _path, _acct ):

        # the apps name
        _app = os.path.basename( _path )

        # the destination
        _dest = "{}{}/{}".format( self.common.backup_path_app, _acct, _app )

        # initialize if we need to
        self.common.backup_init( _dest )

        # run the backup
        self.common.backup_run( _dest, _path )

        # clean up the backup
        self.common.backup_cleanup( _dest )

    # do the work for backing up everything
    def __run_all_file_backup( self, _path ):
        
        # get the path ownership
        _acct = self.common.path_owner_user( _path )

        # the apps name
        _app = os.path.basename( _path )

        # the destination
        _dest = "{}{}/{}".format( self.common.backup_path_app, _acct, _app )

        # initialize if we need to
        self.common.backup_init( _dest )

        # run the backup
        self.common.backup_run( _dest, _path )

        # clean up the backup
        self.common.backup_cleanup( _dest )

    def __run_other_backup( self, _paths ):

        # make sure the paths are set
        if _paths:

            # split the paths string on the comma
            _the_paths = _paths.split( "," )

            # loop over the resulting list
            for _path in _the_paths:

                _abs_path = os.path.expanduser( _path )

                # the destination
                _dest = "{}{}".format( self.common.backup_path_other, _abs_path )

                # initialize if we need to
                self.common.backup_init( _dest )

                # run the backup
                self.common.backup_run( _dest, _abs_path )

                # clean up the backup
                self.common.backup_cleanup( _dest )