#!/usr/bin/python3

# common imports
import os, json, subprocess

class KP_Common:

    def __init__( self ):

        # config file path
        self.config_file = "/root/.kpbr"

        # figure out how many worker threads we want for these tasks
        self.allowed_threads = int( min( 32, os.cpu_count( ) + 4 ) / 2 )

        # set a temporary restore location
        self.tmp_restore_location = "/tmp/restore/"

        # get the machine's hostname
        self.hostname = os.uname( )[1]

        # hold our config variables
        self.key = None
        self.secret = None
        self.hash = None
        self.endpoint = None
        self.bucket = None
        self.region = None
        self.retention = None
        self.name = None
        self.path_start = None
        self.path_for_apps = None
        self.mysql_host = None
        self.mysql_defaults = None
        self.mysql_user = None
        self.mysql_password = None

        # if the config file exists
        if os.path.exists( self.config_file ):

            # open the file as read-only
            with open( self.config_file ) as _settings:

                # load and parse as json
                _config = json.load( _settings )

                # populate
                self.key = _config[0].get( "key" )
                self.secret = _config[0].get( "secret" )
                self.hash = _config[0].get( "hash" )
                self.endpoint = _config[0].get( "endpoint" )
                self.bucket = _config[0].get( "bucket" )
                self.region = _config[0].get( "region" )
                self.retention = _config[0].get( "retention" )
                self.name = _config[0].get( "name" )
                self.path_start = _config[0].get( "path_start" )
                self.path_for_apps = _config[0].get( "path_for_apps" )
                self.mysql_host = _config[0].get( "mysql_host" )
                self.mysql_defaults = _config[0].get( "mysql_defaults" )
                self.mysql_user = _config[0].get( "mysql_user" )
                self.mysql_password = _config[0].get( "mysql_password" )

        # the full backup repository
        self.backup_repo_prefix = "s3://{}/{}/{}/".format( self.endpoint, self.bucket, self.name )
        
        # the backup repo without the self.name
        self.backup_repo = "s3://{}/{}/".format( self.endpoint, self.bucket )

        # the backup path for apps
        self.backup_path_app = "{}apps/".format( self.backup_repo_prefix )

        # the backup path for databases
        self.backup_path_db = "{}database/".format( self.backup_repo_prefix )

        # the backup path for other
        self.backup_path_other = "{}other/".format( self.backup_repo_prefix )

        # format the primary command string, don't allow server site caching
        self.primary_command = "restic --no-lock --no-cache -r {}"

    # run the actual backup
    def backup_run( self, _repo, _path ):

        # setup the full primary command
        _primary = self.primary_command.format( _repo )

        # setup the full backup command
        _cmd = "{} backup {}".format( _primary, _path )

        # run the command
        self.execute( _cmd )

    # run the backup initialization
    def backup_init( self, _repo ):

        # THE repo
        _the_repo = self.primary_command.format( _repo )

        # setup the command to run
        _cmd = "{} snapshots > /dev/null || {} init".format( _the_repo, _the_repo )

        # run the initialization command
        self.execute( _cmd  )

    # run backup clean up
    def backup_cleanup( self, _repo ):

        # THE repo
        _the_repo = self.primary_command.format( _repo )

        # setup the command to run
        _cmd = "{} forget --keep-within {}d --keep-daily {} --prune".format( _the_repo, self.retention, self.retention )

        # run the cleanup command
        self.execute( _cmd  )

    # get the path ownership
    def path_ownership( self, _path ):
        
        # catch an exception
        try:
            from pwd import getpwuid

            # return the user name from the allocated user id. 
            return getpwuid( os.stat( _path ).st_uid ).pw_name
        except:

            # default as root: backup after restore from other machine fails because the user id does not match
            return "root"

    # execute a shell command
    def execute( self, _cmd, _quiet = True ):

        # we don't want the output from this, so run it quietly
        if _quiet:

            # execute the command silently
            subprocess.call( [ _cmd ], shell=True, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL )
        else:

            # we do want the output
            return subprocess.check_output( [ _cmd ], shell=True )

    # our pretty printer ;)
    def kp_print( self, _type, _str ):

        # default to white, or reset
        _reset = "\033[37m"

        # if it's an error
        if _type.lower( ) == "error":

            # red
            _prefix = "\033[91m"
        
        # if it's a success
        if _type.lower( ) == "success":

            # green
            _prefix = "\033[92m"

        # if it's a informative message
        if _type.lower( ) == "info":

            # blue
            _prefix = "\033[94m"

        # print the output based on the selected message type
        print( _prefix + _str + _reset )

    # argument to lower
    def arg_to_lower( self, _arg ):

        if _arg is None:

            # return an empty string
            return ""
        else:

            # return the lowercase string
            return _arg.lower( )

    # concurrent processing of our loops
    def concurrent_process( self, _func, _loop, account = None ):

        # import our concurrency
        from concurrent import futures

        # if none of the other arguments exist
        if account is None:

            # we can fire up the concurrency
            with futures.ThreadPoolExecutor( max_workers=self.allowed_threads ) as _exec:

                [

                    # loop over the paths passed, and process the images in them
                    _exec.submit( _func, _item )
                    for _item in _loop
                ]

        # if it's an account
        else:

            # we can fire up the concurrency
            with futures.ThreadPoolExecutor( max_workers=self.allowed_threads ) as _exec:

                [

                    # loop over the paths passed, and process the images in them
                    _exec.submit( _func, _item, account )
                    for _item in _loop
                ]

    # get the path owner username
    def path_owner_user( self, _path ):
        
        # import our path library
        from pathlib import Path

        # catch an exception
        try:
            
            # get the actual path as an object
            path = Path( _path )

            # return the owner
            return path.owner( )
        except:

            # default as root: backup after restore from other machine fails because the user id does not match
            return "root"

    # get the path owner group name
    def path_owner_group( self, _path ):

        # import our path library
        from pathlib import Path

        # catch an exception
        try:
            
            # get the actual path as an object
            path = Path( _path )

            # return the group
            return path.group( )
        except:

            # default as root: backup after restore from other machine fails because the user id does not match
            return "root"
