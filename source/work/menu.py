#!/usr/bin/python3

# some necessary imports
import json, boto3, os
from datetime import datetime as dt
import dateutil.parser

# import the simple menu module
from simple_term_menu import TerminalMenu

class KP_Menu:

    # fire it up
    def __init__( self ):

        # let's import our common class
        from work.common import KP_Common

        # now throw it into a class wide variable for use
        self.common = KP_Common( )

    # clean us up
    def __del( self ):

        # no longer need common here
        del self.common

    # the snapshot menu
    def snapshot_menu( self, _type, _bu_repo ):

        # setup the restic command we need to run
        _cmd = self.common.primary_command.format( "{} snapshots --json".format( _bu_repo ) )

        # run the command and get the output
        _snapshots = self.common.execute( _cmd, False )

        # let's process the json response
        _resp = json.loads( _snapshots.decode( "utf-8" ) )

        # hold the menu list
        _mlist = []

        # loop over the json
        for _item in _resp:

            # convert the string to a datetime object
            _parsed_dt = dateutil.parser.parse( _item['time'] )

            # set the formatted date/time
            _dt = _parsed_dt.strftime( "%m/%d/%Y %H:%M:%S" )

            # append the parsed time to the selected list            
            _mlist.append( "{}|{}".format( _dt, _item['short_id'] ) )

        # return the menu
        return self.__menu( _mlist, "Select the snapshot to restore: " )

    # the app menu
    def application_menu( self, _bucket, _name, _account, _endpoint ):

        # fire up the boto3 client
        _s3 = boto3.client( "s3", endpoint_url="https://{}".format( _endpoint ) )

        # get the response for the app to be selected
        _app_resp = _s3.list_objects_v2(
            Bucket=_bucket,
            Prefix='{}/{}{}/'.format( _name, "apps/", _account ),
            Delimiter="/" )

        # we don't need the client anymore
        del _s3

        # build out a list of the accounts we're after here
        _app_list = [ os.path.basename( os.path.dirname( i["Prefix"] ) ) for i in _app_resp['CommonPrefixes'] ]

        # return the account list
        return self.__menu( _app_list, "Select the application to restore: " )

    # the account menu
    def account_menu( self, _bucket, _name, _type, _endpoint ):

        # if the backup type is an application
        if _type.lower( ) == "application":

            # set the "end" path
            _endpath = "apps/"
            _sel = "account"
        elif _type.lower( ) == "database":

            # set the "end" path
            _endpath = "database/"
            _sel = "database"
        elif _type.lower( ) == "other":

            # set the "end" path
            _endpath = "other/"
            _sel = "path"

        # fire up the boto3 client
        _s3 = boto3.client( "s3", endpoint_url="https://{}".format( _endpoint ) )

        # check the bucket and path for the selected backup type
        _acct_resp = _s3.list_objects_v2(
                Bucket=_bucket,
                Prefix ='{}/{}'.format( _name, _endpath ),
                Delimiter="/" )

        # we don't need the client anymore
        del _s3

        # the returned account list
        _acct_list = [ os.path.basename( os.path.dirname( i["Prefix"] ) ) for i in _acct_resp['CommonPrefixes'] ]

        # return the account list
        return self.__menu( _acct_list, "Select the {} to restore: ".format( _sel ) )

    # restore method menu
    def restore_method_menu( self ):

        # return it
        return self.__menu( [ "Automatic", "Manual", "Browse" ], "How would you like to restore? " )

    # get the backup type menu
    def backup_type_menu( self ):

        # return it
        return self.__menu( [ "Application", "Database" ], "Select the type of backup you want to restore: " )
    
    # get the location menu
    def location_menu( self ):

        # return it
        return self.__menu( [ "Local", "Remote" ], "Select the backup location: " )

    # process the menu
    def __menu( self, the_list, the_title ):

        # wrap the menu in a try block
        try:

            # the menu
            _menu = TerminalMenu( the_list, 
                title=the_title, 
                search_key=None, 
                show_search_hint=True, 
                show_shortcut_hints=True,
                search_highlight_style={ 'bg_black', 'fg_green' } )

            # show the menu
            _show = _menu.show( )

            # return the selected item
            return the_list[ _show ] or None

        # catch the keyboard interrupt
        except:

            # show a message then exit
            print()
            self.common.kp_print( "info", ( "*" * 52 ) )
            self.common.kp_print( "success", "Exitting the app, please hold." )
            self.common.kp_print( "info", ( "*" * 52 ) )
            time.sleep( 5 )
            sys.exit( )

