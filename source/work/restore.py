#!/usr/bin/python3

# some necessary imports
import sys, os, glob, time, getpass
from datetime import datetime as dt
import dateutil.parser
from work.menu import KP_Menu

class KP_Restore:

    # initialize
    def __init__( self ):

        # let's import our common class
        from work.common import KP_Common

        # now throw it into a class wide variable for use
        self.common = KP_Common( )

        # the temporary mount location, unless specified in "BROWSE"
        self.temp_mount_path = "/tmp/backup-mount/"

        # the menu
        self.menus = KP_Menu( )

        # the s3 key
        self.s3_key = self.common.key

        # the s3 secret
        self.s3_secret = self.common.secret

        # the s3 endpoint
        self.s3_endpoint = self.common.endpoint

        # the s3 bucket
        self.s3_bucket = self.common.bucket

        # the s3 region
        self.s3_region = self.common.region

        # get the server/backup name
        self.bu_name = self.common.name

        # get the associated hash
        self.bu_hash = self.common.hash


    # clean up
    def __del__( self ):

        # clean up the common class
        del self.common

        # clean up the menus class
        del self.menus

    # run it
    def run( self ):

        # show a starting message
        print( "*" * 52 )
        self.common.kp_print( "info", "Please pay attention, we need your input to select\nthe correct backup to be restored." )
        print( "*" * 52 )

        # local or remote backup menu
        _location_menu = self.menus.location_menu( )

        # if remote
        if _location_menu.lower( ) == "remote":

            # the s3 key
            self.s3_key = getpass.getpass( "Enter your S3 API Key: [null]" ) or None

            # the s3 secret
            self.s3_secret = getpass.getpass( "Enter your S3 API Secret: [null]" ) or None

            # the s3 endpoint
            self.s3_endpoint = input( "Enter your S3 Endpoint: [s3.amazonaws.com]" ) or "s3.amazonaws.com"

            # the s3 bucket
            self.s3_bucket = input( "Enter your S3 Bucket: [null]" ) or None

            # the s3 region
            self.s3_region = input( "Enter the S3 region: [us-east-1]" ) or "us-east-1"

            # get the server/backup name
            self.bu_name = input( "Please input the remote backups name: [null]" ) or None

            # get the associated hash
            self.bu_hash = getpass.getpass( "Please input the remote backups hash: [null]" ) or None

            # we need all of the above, otherwise exit with an error message
            if ( not self.s3_key ) or ( not self.s3_secret ) or ( not self.s3_endpoint ) or ( not self.s3_bucket ) or ( not self.s3_region ) or ( not self.bu_name ) or ( not self.bu_hash ):

                print( "*" * 52 )
                self.common.kp_print( "error", "All of this information is required in order\nto connect to your remote backup." )
                print( "*" * 52 )
                sys.exit( )

        # setup the common API environment variables
        os.environ["AWS_ACCESS_KEY_ID"] = self.s3_key
        os.environ["AWS_SECRET_ACCESS_KEY"] = self.s3_secret
        os.environ["AWS_DEFAULT_REGION"] = self.s3_region
        os.environ["RESTIC_PASSWORD"] = self.bu_hash
            
        # backup type menu
        _bu_type = self.menus.backup_type_menu( )

        # restore method menu
        _bu_method = self.menus.restore_method_menu( )

        # the account menu
        _account = self.menus.account_menu( self.s3_bucket, self.bu_name, _bu_type, self.s3_endpoint )

        # hold the app in case we need it later
        _application = None

        # hold the snapshot in case we are not "browsing"
        _snapshot = None

        # if the backup type is application
        if _bu_type.lower( ) == "application":

            # we need to get the application
            _application = self.menus.application_menu( self.s3_bucket, self.bu_name, _account, self.s3_endpoint )

        # setup the repo path
        _repo_path = self.__repo( _bu_type, _account, _application )

        # if we are not browsing
        if _bu_method.lower( ) != "browse":

            # the snapshot menu
            _snapshot = self.menus.snapshot_menu( _bu_type, _repo_path )

        # if we are browsing
        if _bu_method.lower( ) == "browse":

            # mount point input
            _mount_point = input( "Please type in a mount point path: [{}]".format( self.temp_mount_path ) ) or self.temp_mount_path

            # create the mount point if it doesn't exist
            self.common.execute( "mkdir -p {} && chmod -R 755 {}".format( _mount_point, _mount_point ) )

            # the command to run for mounting it
            _cmd = self.common.primary_command.format( "{} mount {}".format( _repo_path, _mount_point ) )
        
            # show a message
            print( "*" * 52 )
            self.common.kp_print( "info", "Your selected backup repo has been has been mounted to:" )
            self.common.kp_print( "success", _mount_point )
            self.common.kp_print( "info", "You must keep this open to utilize the mount point." )
            self.common.kp_print( "info", "Please hold a few seconds for it to finish mounting." )
            self.common.kp_print( "info", "Press CTRL-C to quit." )
            print( "*" * 52 )

            # try to catch the exception
            try:

                # run the mount command
                self.common.execute( _cmd )       

            # catch the keyboard interrupt
            except KeyboardInterrupt:

                # show a message then exit
                self.common.kp_print( "info", "Your mount point has been unmounted." )
                print( "*" * 52 )
                sys.exit( )

        # else if we are restoring
        elif _bu_method.lower( ) == "automatic":

            # split out the id from the selected backup
            _id = _snapshot.split( "|" )

            # if it's an application we're restoring
            if _bu_type.lower( ) == "application":

                # setup the original path
                _orig_path = "{}{}/{}".format(
                    self.common.path_start,
                    _account,
                    self.common.path_for_apps
                )

                # setup the restic command we need to run
                _cmd = self.common.primary_command.format( "{} restore {} --target {}".format( 
                    _repo_path,
                    _id[1], 
                    _orig_path ) )

            elif _bu_type.lower( ) == "database":

                # setup the command we need to run here
                _cmd = self.common.primary_command.format( "{}/ dump {} {}.sql | {}".format( 
                    _repo_path,
                    _id[1], 
                    _account,
                    self.__mysql_command( ) + _account ) )

                # perform the restore
                self.common.execute( _cmd )

            # show a message to hold while restoring
            print( "*" * 52 )
            self.common.kp_print( "info", "Please hold while we run the restore." )
            print( "*" * 52 )

            # execute the command
            self.common.execute( _cmd )

            print( "*" * 52 )
            self.common.kp_print( "success", "Your selected backup has been succesfully restored." )
            print( "*" * 52 )


        # else if we are going to manually restore
        elif _bu_method.lower( ) == "manual":

            # prompt for the location to copy the backup to
            _location = input( "Type in the path you would like to copy the restore point to: [{}]".format( self.common.tmp_restore_location ) ) or self.common.tmp_restore_location

            # create the location directory
            self.common.execute( "mkdir -p {} && chmod -R 755 {}".format( _location, _location ) )

            # get the snapshot id
            _id = _snapshot.split( "|" )

            # setup the command to run
            _cmd = self.common.primary_command.format( "{} restore {} --target {}".format( 
                _repo_path,
                _id[1], 
                _location ) )

            # show a message to hold while restoring
            print( "*" * 52 )
            self.common.kp_print( "info", "Please hold while we run the restore." )
            print( "*" * 52 )

            # execute the command
            self.common.execute( _cmd )

            print( "*" * 52 )
            self.common.kp_print( "info", "Your selected backup has been restored to the following location:" )
            self.common.kp_print( "success", _location )
            print( "*" * 52 )

        # remove the environment variables
        del os.environ['AWS_ACCESS_KEY_ID']
        del os.environ['AWS_SECRET_ACCESS_KEY']
        del os.environ['AWS_DEFAULT_REGION']
        del os.environ['RESTIC_PASSWORD']

    # format the mysql command
    def __mysql_command( self ):

        # hold our command
        _cmd = "mysql {} "

        # let's see if we have a defaults file
        if self.common.mysql_defaults:

            # we do, return the string with the defaults file appended
            return _cmd.format( "--defaults-file={}".format( self.common.mysql_defaults ) )
        
        # we don't, so test for a username and password
        elif ( self.common.mysql_user is not None ) and ( self.common.mysql_password is not None ):

            # we do, return the string with the user and password appended
            return _cmd.format( "-u {} -p{}".format( self.common.mysql_user, self.common.mysql_password ) )

        # we have nothing, so give it a shot.
        else:

            # return the string with no formatting
            return _cmd

    # create the repo string
    def __repo( self, _type, _account, _application = None ):

        # if the backup type is an application
        if _type.lower( ) == "application":

            # set the repo path
            _repo_path = "{}{}/apps/{}/{}".format( "s3://{}/{}/".format( self.s3_endpoint, self.s3_bucket ), 
                self.bu_name, 
                _account, 
                _application )
        elif _type.lower( ) == "database":

            # set the repo path
            _repo_path = "{}{}/database/{}".format( "s3://{}/{}/".format( self.s3_endpoint, self.s3_bucket ), 
                self.bu_name, 
                _account )
        elif _type.lower( ) == "other":

            # UNUSED FOR NOW
            # set the repo path
            _repo_path = "other/"

        return _repo_path
