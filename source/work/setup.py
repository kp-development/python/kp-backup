#!/usr/bin/python3

# some necessary imports
import os, time, sys

class KP_Setup:

    # initialize this class
    def __init__( self ):

        # let's import our common class
        from work.common import KP_Common

        # now throw it into a class wide variable for use
        self.common = KP_Common( )

    # destroy us
    def __del__( self ):

        # clean up the common class
        del self.common

    # run  the setup
    def run( self ):

        print( "*" * 52 )
        self.common.kp_print( "info", "Please hold while we setup your system." )
        print( "*" * 52 )
        time.sleep( 5 )

        # update the system apt cache
        self.common.execute( "apt-get update" )

        # make sure pyton3 is python3.8
        # self.common.execute( "update-alternatives --set python3 /usr/bin/python3.8" )

        # check if curl is installed, if not, install it
        self.common.execute( "apt-get -y install curl" )

        # check if restic is installed if not, install it
        self.common.execute( "apt-get -y install restic" )

        # install pip if it is not currently installed
        self.common.execute( "apt-get -y install python3-pip" )

        # install the gcc and python3-dev
        self.common.execute( "apt-get -y install gcc python3-dev" )

        # install python invoke, if it is not currently installed
        self.common.execute( "apt-get -y install python3-invoke" )

        # libmysqlclient-dev
        self.common.execute( "apt-get -y install libmysqlclient-dev" )

        # check if the mysql python module is installed
        self.common.execute( "apt-get -y install python3-mysqldb" )

        # check if the python3 boto module is installed
        self.common.execute( "apt-get -y install python3-boto python3-boto3" )

        # make sure pip is up to date
        self.common.execute( "python3 -m pip install --user --upgrade pip" )

        # install/upgrade python-dateutil module
        self.common.execute( "python3 -m pip install --upgrade python-dateutil" )

        # install upgrade the simple terminal menu
        self.common.execute( 'python3 -m pip install --upgrade simple_term_menu' )

        # insall/upgrade the requests module
        self.common.execute( "python3 -m pip install --upgrade requests" )

        # insall/upgrade the requests module
        self.common.execute( "python3 -m pip install --upgrade boto" )

        # insall/upgrade the requests module
        self.common.execute( "python3 -m pip install --upgrade boto3" )

        # now that updates/upgrades are done, clean and remove older package versions
        self.common.execute( "apt-get -y autoremove" )
        self.common.execute( "apt-get -y clean" )
        self.common.execute( "apt-get -y autoclean" )
        
        # show a message
        self.common.kp_print( "info", "The setup has completed.\nYou will now be prompted to configure the app." )

        # run the config
        from work.config import KP_Config
        _config = KP_Config( )

        # run the configurator
        _config.run( )

        # clean up
        del _config
